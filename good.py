import datetime
import requests
import json
import os
import sys
from selenium import webdriver
from collections import defaultdict
from PIL import ImageFont
import inkyphat

FAKE = True

def main():
    pages = {'2014': 7375, '2015': 23508, '2016': 17696, '2017': 19706, '2018': 9388}
    books = {"2018":33,"2017":64,"2016":47,"2015":75,"2014":19}
    if not FAKE:
        pages, books = get_goodreads_stats()
    stats = get_stats(pages, books)
    draw(stats)


def get_goodreads_stats():
    payload = {
      'email': os.environ.get('EMAIL'),
      'password': os.environ.get('PASSW'),
    }
    
    LOGIN_URL= 'https://www.goodreads.com/user/sign_in'
    
    driver = webdriver.PhantomJS()
    driver.get(LOGIN_URL)
    
    username = driver.find_element_by_id('user_email')
    password = driver.find_element_by_id('user_password')
    
    username.send_keys(payload['email'])
    password.send_keys(payload['password'])
    
    form = driver.find_element_by_name('sign_in')
    form.submit()
    
    driver.get('https://www.goodreads.com/review/stats/24381583#pages')
    pages = driver.execute_script('return page_stats')
    books = driver.execute_script('return year_stats')
    return pages, books

def get_stats(pages, books):
    size = 4
    col_w, col_h = 40, 32
    x_start, y_start = 53, 10
    row_offsets = [0,5,0]

    rows = ["year", "books", "pages"]
    coords = defaultdict(list)

    for column in range(0, size):
        for row in range(0, len(rows)):
            coords[rows[row]].append([
                row_offsets[row] + x_start + (column*col_w),
                y_start + (row*col_h),
            ])
    _stats = []
    for idx, year in enumerate(list(reversed(sorted(books.keys())))[:size]):
        page_count = pages[year]
        book_count = books[year]
        _stats.append({
            'year': [str(year), coords['year'][idx]],
            'books': [str(book_count), coords['books'][idx]],
            'pages': [str(page_count), coords['pages'][idx]],
        })
    return _stats

def draw(stats):
    colour = 'red'
    inkyphat.set_colour(colour)
    inkyphat.set_border(inkyphat.WHITE)
    inkyphat.set_image("./background.png")

    draw_row('year', stats, 12)
    draw_row('books', stats, 18)
    draw_row('pages', stats, 12)

    inkyphat.show()

def draw_row(row, stats, size):
    #font = ImageFont.truetype(inkyphat.fonts.PressStart2P, size)
    font = ImageFont.truetype(inkyphat.fonts.FredokaOne, size)
    #font = ImageFont.truetype(inkyphat.fonts.AmaticSC, size)
    for year in stats:
        count = year[row][0]
        x,y = year[row][1]
        w, h = font.getsize(count)
        inkyphat.text((x, y), count, inkyphat.WHITE, font)

if __name__ == "__main__":
    main()

