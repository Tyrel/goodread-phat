from collections import defaultdict

pages = {"2014": 7375, "2015": 23508, "2016": 17696, "2017": 19706, "2018": 9388}
books = {"2018": 33,"2017":64,"2016":47,"2015":75,"2014":19}


def main():
    years = ['2018','2017','2016','2015','2014']

    coords = defaultdict(list)

    columns = ["year", "books", "pages"]

    start_x, start_y = 12, 25
    column_offset = [0,70,140]
    year_offset = 15

    for idy, year in enumerate(years):
        for idx, column in enumerate(columns):
            coords[year].append((
        		start_x + column_offset[idx], 
        		start_y + (year_offset * idy)
    		 ))
    print coords

if __name__=="__main__":
    main()



